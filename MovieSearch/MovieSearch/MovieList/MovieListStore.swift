//
//  MovieListStore.swift
//  MovieSearch
//
//  Created by PIYARAT PEARODWONG on 1/26/22.
//  Copyright (c) 2022 ___ORGANIZATIONNAME___. All rights reserved.
//

import UIKit
import CoreData

/*
 
 The MovieListStore class implements the MovieListStoreProtocol.
 
 The source for the data could be a database, cache, or a web service.
 
 You may remove these comments from the file.
 
 */

class MovieListStore: MovieListStoreProtocol {
    let context = (UIApplication.shared.delegate as! AppDelegate).persistentContainer.viewContext
    func getMovieList(search: String,page: Int,_ completion: @escaping (Result<MovieResult>) -> Void) {
        let key = "15b4e67f5b6884dd14a0c45d3998e9b92e613720"
        let movieURL = "https://scb-movies-api.herokuapp.com/api/movies/search"
        
        let originalString = "\(movieURL)?query=\(search)&page=\(page)"
        let urlString = originalString.addingPercentEncoding(withAllowedCharacters: .urlQueryAllowed)
        if let url = URL(string: urlString ?? ""){
            print(url)
            let session = URLSession(configuration: .default)
            var request = URLRequest(url: url)
            request.httpMethod = "GET"
            request.setValue(key, forHTTPHeaderField:"api-key")
            do{
                let task = session.dataTask(with: request) { data, response, error in
                    if error != nil{
                        return
                    }
                    else{
                        let decoder = JSONDecoder()
                        if let safeData = data{
                            guard let data = try? decoder.decode(MovieResult.self, from: safeData)
                            else{
                                return
                            }
                            //save SearchData
                            DispatchQueue.main.async {
                                if  data.results.count != 0 {
                                    self.checkSameData(search)
                                    self.checkIndexCount()
                                    let newSearch = PreviousSearch(context: self.context)
                                    newSearch.searchLabel = search
                                    newSearch.date = Date()
                                    self.saveData()
                                }
                                completion(.success(data))
                            }
                            
                        }
                        
                    }
                    
                }
                task.resume()
            }
            
        }
        
    }
    
    //MARK: - Data Manipulation Methods
    
    func saveData(){
        do{
            try context.save()
        }catch{
            print("Error saving context \(error)")
        }
    }
    
    func checkSameData(_ search:String) {
        let fetchRequest: NSFetchRequest<PreviousSearch> = PreviousSearch.fetchRequest()
        fetchRequest.predicate = NSPredicate(format: "searchLabel = %@ ",search)
        do {
            if let results = try? context.fetch(fetchRequest){
                if results.count != 0 {
                    for result in results {
                        context.delete(result)
                    }
                }
            }
            saveData()
        }catch{
            print("Fetch Failed: \(error)")
        }
    }
    
    func checkIndexCount(){
        let fetchRequest: NSFetchRequest<PreviousSearch> = PreviousSearch.fetchRequest()
        let count = try! context.count(for: fetchRequest)
        if count >= 5 {
            deleleOldestData()
        }
    }
    
    func deleleOldestData(){
        let fetchRequest: NSFetchRequest<PreviousSearch> = PreviousSearch.fetchRequest()
        fetchRequest.sortDescriptors = [NSSortDescriptor(key: "date", ascending: true)]
        do {
            if let results = try? context.fetch(fetchRequest){
                context.delete(results[0])
            }
        }catch{
            print("Fetch Failed: \(error)")
        }
        
    }
    
}
