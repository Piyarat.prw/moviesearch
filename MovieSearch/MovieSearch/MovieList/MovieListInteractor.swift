//
//  MovieListInteractor.swift
//  MovieSearch
//
//  Created by PIYARAT PEARODWONG on 1/26/22.
//  Copyright (c) 2022 ___ORGANIZATIONNAME___. All rights reserved.
//

import UIKit

protocol MovieListInteractorInterface {
    func getMovieList(request: MovieList.GetMovieList.Request)
    func setSelectedIndex(request: MovieList.SetSelectedIndex.Request)
    
    var searchData: String? { get set }
    var selectedMovie: Movie? { get }
    var searchPage: Int {get}
    var totalPage: Int {get}
}

class MovieListInteractor: MovieListInteractorInterface {
    var presenter: MovieListPresenterInterface!
    var worker: MovieListWorker?
    var searchData: String?
    var searchPage: Int = 1
    var totalPage: Int = 1
    var movieResult: MovieResult?
    var movies: [Movie] = []
    var selectedMovie: Movie?
    
    // MARK: - Business logic
    
    func getMovieList(request: MovieList.GetMovieList.Request) {
        if let search = searchData {
            if searchPage <= totalPage{
                worker?.getMovieList(search: search, page: searchPage){ [weak self] apiResponse in
                    switch apiResponse {
                    case .success(let movieResult):
                        self?.movieResult = movieResult
                        for element in movieResult.results{
                            self?.movies.append(element)
                        }
                        self?.totalPage = movieResult.total_pages
                        self?.searchPage = movieResult.page + 1
                        let response = MovieList.GetMovieList.Response(movieList: self?.movies ?? [])
                        self?.presenter.presentMovies(response: response)
                    case .failure(let error):
                        print(error)
                    }
                }
            }
        }
        
    }
    
    func setSelectedIndex(request: MovieList.SetSelectedIndex.Request) {
        selectedMovie = movies[request.index]
        let response = MovieList.SetSelectedIndex.Response()
        presenter.presentSelectedIndex(response: response)
    }
}
