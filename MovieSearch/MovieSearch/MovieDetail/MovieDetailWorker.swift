//
//  MovieDetailWorker.swift
//  MovieSearch
//
//  Created by PIYARAT PEARODWONG on 1/27/22.
//  Copyright (c) 2022 ___ORGANIZATIONNAME___. All rights reserved.
//

import UIKit

protocol MovieDetailStoreProtocol {
    func checkData(id: Int, _ completion: @escaping (Result<String>) -> Void)
    func saveData(movie: Movie, _ completion: @escaping (Result<String>) -> Void)
    func deleteData(id: Int, _ completion: @escaping (Result<String>) -> Void)
}

class MovieDetailWorker {
    
    var store: MovieDetailStoreProtocol
    
    init(store: MovieDetailStoreProtocol) {
        self.store = store
    }
    
    // MARK: - Business Logic
    
    func checkData(id: Int,_ completion: @escaping (Result<String>) -> Void) {
        store.checkData(id: id){ result in completion(result)
        }
    }
    func saveData(movie: Movie, _ completion: @escaping (Result<String>) -> Void){
        store.saveData(movie: movie){ result in completion(result)
        }
    }
    func deleteData(id: Int,_ completion: @escaping (Result<String>) -> Void) {
        store.deleteData(id: id){ result in completion(result)
        }
    }
}
