//
//  MovieDetailStore.swift
//  MovieSearch
//
//  Created by PIYARAT PEARODWONG on 1/27/22.
//  Copyright (c) 2022 ___ORGANIZATIONNAME___. All rights reserved.
//

import UIKit
import CoreData

/*
 
 The MovieDetailStore class implements the MovieDetailStoreProtocol.
 
 The source for the data could be a database, cache, or a web service.
 
 You may remove these comments from the file.
 
 */

class MovieDetailStore: MovieDetailStoreProtocol {
    let context = (UIApplication.shared.delegate as! AppDelegate).persistentContainer.viewContext
    
    func saveData(movie: Movie, _ completion: @escaping (Result<String>) -> Void) {
        let newFavoriteData = FavoriteList(context: self.context)
        newFavoriteData.id = Int64(movie.id)
        newFavoriteData.title = movie.title
        newFavoriteData.release_date = movie.release_date
        newFavoriteData.poster_path = movie.poster_path
        newFavoriteData.vote_average = movie.vote_average ?? 0
        newFavoriteData.overview = movie.overview
        do{
            try context.save()
            completion(.success("Unfavorite"))
        }catch{
            print("Error saving context \(error)")
        }
    }
    
    func deleteData(id: Int, _ completion: @escaping (Result<String>) -> Void) {
        let fetchRequest: NSFetchRequest<FavoriteList> = FavoriteList.fetchRequest()
        fetchRequest.predicate = NSPredicate(format: "id = \(id)")
        do {
            if let results = try? context.fetch(fetchRequest){
                if results.count != 0 {
                    for result in results {
                        context.delete(result)
                    }
                }
            }
            do{
                try context.save()
                completion(.success("Favorite"))
            }catch{
                print("Error saving context \(error)")
            }
        }catch{
            print("Fetch Failed: \(error)")
        }
    }
    
    func checkData(id: Int,_ completion: @escaping (Result<String>) -> Void) {
        let fetchRequest: NSFetchRequest<FavoriteList> = FavoriteList.fetchRequest()
        fetchRequest.predicate = NSPredicate(format: "id = \(id) ")
        do {
            if let results = try? context.fetch(fetchRequest){
                if results.count != 0 {
                    completion(.success("Unfavorite"))
                }else {
                    completion(.success("Favorite"))
                }
            }
        }catch{
            print("Fetch Failed: \(error)")
        }
    }
}
