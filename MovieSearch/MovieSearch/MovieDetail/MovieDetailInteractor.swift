//
//  MovieDetailInteractor.swift
//  MovieSearch
//
//  Created by PIYARAT PEARODWONG on 1/27/22.
//  Copyright (c) 2022 ___ORGANIZATIONNAME___. All rights reserved.
//

import UIKit

protocol MovieDetailInteractorInterface {
    func setupData(request: MovieDetail.SetupData.Request)
    func checkFavorite(request: MovieDetail.SetFavoriteStatus.Request)
    func setFavorite(request: MovieDetail.SetFavoriteStatus.Request)
    var movie: Movie? { get set }
}

class MovieDetailInteractor: MovieDetailInteractorInterface {
    var presenter: MovieDetailPresenterInterface!
    var worker: MovieDetailWorker?
    var movie: Movie?
    var label: String?
    
    // MARK: - Business logic
    
    func setupData(request: MovieDetail.SetupData.Request) {
        if let movieData = movie{
            let response = MovieDetail.SetupData.Response(movie: movieData)
            presenter.presentMovieDetail(response: response)
        }
    }
    func checkFavorite(request: MovieDetail.SetFavoriteStatus.Request){
        if let movieData = movie {
            worker?.checkData(id: movieData.id){ [weak self] coreDataResponse in switch coreDataResponse {
            case .success(let label):
                self?.label = label
                let response = MovieDetail.SetFavoriteStatus.Response(label: label)
                self?.presenter.presentFavoriteStatus(response: response)
            case .failure(let error):
                print(error)
            }
            }
        }
    }
    func setFavorite(request: MovieDetail.SetFavoriteStatus.Request){
        if let movieData = movie {
            if label == "Favorite"{
                worker?.saveData(movie: movieData){ [weak self] coreDataResponse in switch coreDataResponse {
                case .success(let label):
                    self?.label = label
                    let response = MovieDetail.SetFavoriteStatus.Response(label: label)
                    self?.presenter.presentFavoriteStatus(response: response)
                case .failure(let error):
                    print(error)
                }
                }
            }else if label == "Unfavorite"{
                worker?.deleteData(id: movieData.id){ [weak self] coreDataResponse in switch coreDataResponse {
                case .success(let label):
                    self?.label = label
                    let response = MovieDetail.SetFavoriteStatus.Response(label: label)
                    self?.presenter.presentFavoriteStatus(response: response)
                case .failure(let error):
                    print(error)
                }
                }
            }
        }
    }
}
