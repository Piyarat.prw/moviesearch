//
//  MovieDetailPresenter.swift
//  MovieSearch
//
//  Created by PIYARAT PEARODWONG on 1/27/22.
//  Copyright (c) 2022 ___ORGANIZATIONNAME___. All rights reserved.
//

import UIKit

protocol MovieDetailPresenterInterface {
    func presentMovieDetail(response: MovieDetail.SetupData.Response)
    func presentFavoriteStatus(response: MovieDetail.SetFavoriteStatus.Response)
}

class MovieDetailPresenter: MovieDetailPresenterInterface {
    weak var viewController: MovieDetailViewControllerInterface!
    
    // MARK: - Presentation logic
    
    func presentMovieDetail(response: MovieDetail.SetupData.Response) {
        let title = response.movie.title
        let poster_path = response.movie.poster_path ?? ""
        var average : String = ""
        if let vote_average = response.movie.vote_average{
            average = String(vote_average)
        }
        let overview = response.movie.overview ?? ""
        let viewModel = MovieDetail.SetupData.ViewModel(title: title, poster_path: poster_path, average: average, overview: overview)
        viewController.displayMovieDetail(viewModel: viewModel)
    }
    
    func presentFavoriteStatus(response: MovieDetail.SetFavoriteStatus.Response) {
        let label = response.label
        let viewModel = MovieDetail.SetFavoriteStatus.ViewModel(label: label)
        viewController.displayFavoriteButtonDetail(viewModel: viewModel)
    }
}
