//
//  SearchMovieStore.swift
//  MovieSearch
//
//  Created by PIYARAT PEARODWONG on 1/26/22.
//  Copyright (c) 2022 ___ORGANIZATIONNAME___. All rights reserved.
//

import UIKit
import CoreData

/*

 The SearchMovieStore class implements the SearchMovieStoreProtocol.

 The source for the data could be a database, cache, or a web service.

 You may remove these comments from the file.

 */

class SearchMovieStore: SearchMovieStoreProtocol {
  func getSearch(_ completion: @escaping (Result<[String]>) -> Void) {
      let context = (UIApplication.shared.delegate as! AppDelegate).persistentContainer.viewContext
      let request:NSFetchRequest<PreviousSearch> = PreviousSearch.fetchRequest()
      request.sortDescriptors = [NSSortDescriptor(key: "date", ascending: false)]
      do{
          let results = try context.fetch(request)
          var searchList = [String]()
          for result in results {
              searchList.append(result.searchLabel!)
          }
          completion(.success(searchList))
      }catch{
          print(error)
      }
  }
}
