//
//  FavoriteMovieModels.swift
//  MovieSearch
//
//  Created by PIYARAT PEARODWONG on 1/27/22.
//  Copyright (c) 2022 ___ORGANIZATIONNAME___. All rights reserved.
//

import UIKit

struct FavoriteMovie {
  /// This structure represents a use case
  struct GetFavoriteList {
    /// Data struct sent to Interactor
    struct Request {}
    /// Data struct sent to Presenter
      struct Response {
          let movieList: [Movie]
      }
      /// Data struct sent to ViewController
      struct ViewModel {
          struct MovieViewModel{
              let id: Int
              let title: String
              let poster_path: String
              let overview: String
              let release_date: String
              let average: String
          }
          var movieViewModels: [MovieViewModel]
      }
  }
  struct SetSelectedIndex{
      struct Request {
        let index: Int
      }
      struct Response {
          
      }
      struct ViewModel {
         
      }
  }
}
