//
//  FavoriteMovieInteractorTests.swift
//  MovieSearch
//
//  Created by PIYARAT PEARODWONG on 1/28/22.
//  Copyright (c) 2022 ___ORGANIZATIONNAME___. All rights reserved.
//

@testable import MovieSearch
import XCTest

class FavoriteMovieInteractorTests: XCTestCase {
    
    // MARK: - Subject under test
    
    var interactor: FavoriteMovieInteractor!
    
    // MARK: - Test lifecycle
    
    override func setUp() {
        super.setUp()
        setupFavoriteMovieInteractor()
    }
    
    override func tearDown() {
        super.tearDown()
    }
    
    // MARK: - Test setup
    
    func setupFavoriteMovieInteractor() {
        interactor = FavoriteMovieInteractor()
    }
    
    // MARK: - Test doubles
    
    class FavoriteMovieWorkerTest : FavoriteMovieWorker{
        var getFavoriteListCalled: Bool = false
        var getFavoriteListResult: Result<[Movie]>?
        
        override func getFavoriteList(_ completion: @escaping (Result<[Movie]>) -> Void) {
            getFavoriteListCalled = true
            let movies: [Movie] = [
            Movie(id: 1, poster_path: "/kgwjIb2JDHRhNk13lmSxiClFjVk.jpg", title: "Frozen1", release_date: "2013-11-20", overview: "Young princess Anna of Arendelle dreams about finding true love at her sister Elsa’s coronation.", vote_average: 10),
            Movie(id: 2, poster_path: "/mINJaa34MtknCYl5AjtNJzWj8cD.jpg", title: "Frozen2", release_date: "2019-11-20", overview: "Elsa, Anna, Kristoff and Olaf head far into the forest to learn the truth about an ancient mystery of their kingdom.", vote_average: 7.3)
            ]
            let movieResult: Result<[Movie]> = Result.success(movies)
            getFavoriteListResult = movieResult
            completion(movieResult)
        }
    }
    
    class OutputTest : FavoriteMoviePresenterInterface{
        var presentMoviesCalled: Bool = false
        var presentMoviesResponse: FavoriteMovie.GetFavoriteList.Response?
        func presentMovies(response: FavoriteMovie.GetFavoriteList.Response) {
            presentMoviesCalled = true
            presentMoviesResponse = response
        }
        
        var presentSelectedIndexCalled: Bool = false
        var presentSelectedIndexResponse: FavoriteMovie.SetSelectedIndex.Response?
        func presentSelectedIndex(response: FavoriteMovie.SetSelectedIndex.Response) {
            presentSelectedIndexCalled = true
            presentSelectedIndexResponse = response
        }
        
    }
    
    // MARK: - Tests
    
    func testGetFavoriteList(){
        // Given
        let workerTest: FavoriteMovieWorkerTest = FavoriteMovieWorkerTest(store: FavoriteMovieStore())
        interactor.worker = workerTest
        
        let outputTest = OutputTest()
        interactor.presenter = outputTest
        
        let request = FavoriteMovie.GetFavoriteList.Request()
        
        // When
        interactor.getFavoriteList(request: request)
        
        // Then
        XCTAssert(workerTest.getFavoriteListCalled)
        if let result = workerTest.getFavoriteListResult {
            switch result {
            case .success(let data):
                XCTAssertEqual(data.count, 2)
                XCTAssertEqual(interactor.movies.count, 2)
            case .failure(_):
                XCTFail("result is failure")
            }
        }
        
        XCTAssert(outputTest.presentMoviesCalled)
        if let response = outputTest.presentMoviesResponse{
            XCTAssertEqual(response.movieList.count, 2)
        }
    }
    
    func testSetSelectedIndex() {
        // Given
        let outputTest = OutputTest()
        interactor.presenter = outputTest
        
        let movie = Movie(id: 1, poster_path: "/kgwjIb2JDHRhNk13lmSxiClFjVk.jpg", title: "Frozen1", release_date: "2013-11-20", overview: "Young princess Anna of Arendelle dreams about finding true love at her sister Elsa’s coronation.", vote_average: 10)
        interactor.movies = [movie]
        
        let request = FavoriteMovie.SetSelectedIndex.Request(index: 0)
        
        // When
        interactor.setSelectedIndex(request: request)
        // Then
        XCTAssert(outputTest.presentSelectedIndexCalled)
        XCTAssertEqual(interactor.selectedMovie?.id,1)
    }
}
